import { User } from '../entity/user.entity';
import { UsersService } from '../service/user/user.service';
export declare class UsersController {
    private usersService;
    constructor(usersService: UsersService);
    findAll(): Promise<User[]>;
    get(params: any): Promise<User[]>;
    create(user: User): User;
    update(user: User): Promise<void>;
    deleteUser(params: any): Promise<void>;
}
