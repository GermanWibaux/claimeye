import { User } from 'src/user/entity/user.entity';
import { Repository } from 'typeorm';
export declare class UsersService {
    private usersRepository;
    constructor(usersRepository: Repository<User>);
    createUser(user: User): User;
    getUsers(): Promise<User[]>;
    getUser(_id: number): Promise<User[]>;
    updateUser(user: User): Promise<void>;
    deleteUser(user: User): Promise<void>;
}
