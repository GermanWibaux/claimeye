export declare class User {
    id: number;
    username: string;
    email: string;
    last_name: string;
}
