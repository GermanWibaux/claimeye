import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length:25})
    username:string;

    @Column({length:25})
    email:string;

    @Column({length:25})
    last_name:string;
}