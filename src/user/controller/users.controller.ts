import { Body, Controller, Delete, Get, Param, Post, Put, Response } from '@nestjs/common';
import { UserDto } from '../dto/user.dto';
import { User } from '../entity/user.entity';
import { UsersService } from '../service/user/user.service';

@Controller('users')
export class UsersController {
    constructor (private usersService: UsersService) {}

    // @Post('/')
    // create(@Body() user: UserDto): UserDto {
    //     return this.usersService.create(user);
    // }

    @Get('/')
    findAll(): Promise<User[]> {
        // console.log(res);
        return this.usersService.getUsers();
    }
    
    // constructor(private service: UsersService) { }

    @Get(':id')
    get(@Param() params) {
        return this.usersService.getUser(params.id);
    }

    @Post()
    create(@Body() user: User) {
        return this.usersService.createUser(user);
    }

    @Put()
    update(@Body() user: User) {
        return this.usersService.updateUser(user);
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.usersService.deleteUser(params.id);
    }

}
