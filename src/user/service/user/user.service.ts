import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserDto } from 'src/user/dto/user.dto';
import { User } from 'src/user/entity/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

    // public users: UserDto[] = [];
    // findall(): UserDto[] {
    //     return this.users;
    // }

    constructor(@InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(User) private usersViewRepository: Repository<User>) { }

    createUser(user: User): User {
        this.usersRepository.create(user);
        return user;
    }

    async getUsers(): Promise<User[]> {
        return await this.usersViewRepository.find();
    }

    async getUser(_id: number): Promise<User[]> {
        return await this.usersRepository.find({
            select: ["username", "email", "last_name"],
            where: [{ "id": _id }]
        });
    }

    async updateUser(user: User) {
        this.usersRepository.save(user)
    }

    async deleteUser(user: User) {
        this.usersRepository.delete(user);
    }

}
